# The code for `add_arrow` modified from
# https://stackoverflow.com/a/34018322/2520903.  The original code added a
# single arrow along a plotted line.  The modification adds `n_arrows` arrows
# along a plotted line.

import numpy as np

def add_arrow(line, n_arrows=1, direction='right', size=15, color=None):
    """
    add arrows to a line.

    line:       Line2D object
    direction:  'left' or 'right'
    n_arrows:   number of arrows to be equally spaced horizontally
                over line
    size:       size of the arrow in fontsize points
    color:      if None, line color is taken.
    """
    if color is None:
        color = line.get_color()

    xdata = line.get_xdata()
    ydata = line.get_ydata()

    endpoints = np.linspace(xdata.min(), xdata.max(), n_arrows+1)
    for i in range(n_arrows):
        position = xdata[(xdata>=endpoints[i]) & (xdata<=endpoints[i+1])].mean()
        # find closest index
        start_ind = np.argmin(np.absolute(xdata - position))
        if direction == 'right':
            end_ind = start_ind + 1
        else:
            end_ind = start_ind - 1

        line.axes.annotate('',
            xytext=(xdata[start_ind], ydata[start_ind]),
            xy=(xdata[end_ind], ydata[end_ind]),
            arrowprops=dict(arrowstyle="->", color=color),
            size=size
        )

def r_angle_arrows(ax, coords, direction='nw', size=0.1, color='k'):
    """
    plot right angle arrows for phase diagram. (Default aspect ratio
    appears to be 2/3.)
    
    ax:     plt.Axes object
    direction:  'nw', 'ne', 'sw', 'se'
    size:       length of arms in terms of fraction of vertical
                dimension
    color:      default black

    *Ideally run after any xlim and ylim statements otherwise arrow
     dimensions may be wrong.*
    """
    
    x_len = ax.get_xlim()[1]-ax.get_xlim()[0]
    y_len = ax.get_ylim()[1]-ax.get_ylim()[0]
    
    if direction=='nw':
        dx=(-2/3*x_len*size,0)
        dy=(0,y_len*size)
    elif direction=='ne':
        dx=(2/3*x_len*size,0)
        dy=(0,y_len*size)
    elif direction=='sw':
        dx=(-2/3*x_len*size,0)
        dy=(0,-y_len*size)
    elif direction=='se':
        dx=(2/3*x_len*size,0)
        dy=(0,-y_len*size)
    else:
        print("Direction must be one of 'nw, 'ne', 'sw', 'se'.")
    
    ax.annotate("",
                xy=(coords[0]+dx[0],coords[1]+dx[1]), xycoords='data',
                xytext=coords, textcoords='data',
                arrowprops=dict(arrowstyle="->", shrinkA=0)
               )
    ax.annotate("",
                xy=(coords[0]+dy[0],coords[1]+dy[1]), xycoords='data',
                xytext=coords, textcoords='data',
                arrowprops=dict(arrowstyle="->", shrinkA=0)
               )
